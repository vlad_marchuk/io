package com.iostreams.wrappers;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileWriterWrapper {
    public FileWriter getFileWriter(File file, boolean add) throws IOException {
        return new FileWriter(file, add);
    }
}
